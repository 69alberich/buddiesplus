<?php namespace Qchsoft\Buddiesplus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftBuddiesplusCities extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_buddiesplus_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->integer('country_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_buddiesplus_cities');
    }
}
