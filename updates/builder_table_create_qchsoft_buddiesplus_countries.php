<?php namespace Qchsoft\Buddiesplus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftBuddiesplusCountries extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_buddiesplus_countries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_buddiesplus_countries');
    }
}
