<?php namespace Qchsoft\Buddiesplus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration108 extends Migration
{
    public function up()
    {
        
        Schema::table('lovata_orders_shopaholic_user_addresses', function ($table) {
           // $table->integer('city_id', 11)->nullable();
            $table->integer('city_id')->unsigned();

            //$table->foreign('city_id')->references('id')->on('users');
        });
        //lovata_orders_shopaholic_user_addresses
        // Schema::create('qchsoft_buddiesplus_table', function($table)
        // {
        // });
    }

    public function down()
    {
        Schema::table('lovata_orders_shopaholic_user_addresses', function ($table) {
            $table->dropColumn('city_id');
        });
    }
}