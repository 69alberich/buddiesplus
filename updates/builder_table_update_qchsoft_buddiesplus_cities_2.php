<?php namespace Qchsoft\Buddiesplus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftBuddiesplusCities2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_buddiesplus_cities', function($table)
        {
            $table->renameColumn('country_id', 'state_id');
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_buddiesplus_cities', function($table)
        {
            $table->renameColumn('state_id', 'country_id');
        });
    }
}
