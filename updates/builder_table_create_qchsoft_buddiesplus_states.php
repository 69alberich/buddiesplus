<?php namespace Qchsoft\Buddiesplus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftBuddiesplusStates extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_buddiesplus_states', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('country_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_buddiesplus_states');
    }
}
