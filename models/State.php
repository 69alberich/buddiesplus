<?php namespace Qchsoft\Buddiesplus\Models;

use Model;

/**
 * Model
 */
class State extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_buddiesplus_states';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'country' => ['Qchsoft\Buddiesplus\Models\Country', 'key' => 'country_id']
        //VARIABLE - RUTA DEL MODELO - KEY--> CLAVE-FORANEA EN MI TABLA
       ];
}
