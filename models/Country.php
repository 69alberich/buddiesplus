<?php namespace Qchsoft\Buddiesplus\Models;

use Model;

/**
 * Model
 */
class Country extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_buddiesplus_countries';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

}
