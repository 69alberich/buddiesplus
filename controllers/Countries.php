<?php namespace Qchsoft\Buddiesplus\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use System\Classes\SettingsManager;

class Countries extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Qchsoft.buddiesplus', 'buddiesplus-qchsoft-menu-countries');
        parent::__construct();

    }
}
