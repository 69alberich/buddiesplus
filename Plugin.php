<?php namespace Qchsoft\Buddiesplus;

use System\Classes\PluginBase;
use Backend;
use Event;
use Qchsoft\BuddiesPlus\Classes\Event\UserAddressModelHandler;
use Qchsoft\BuddiesPlus\Classes\Event\UsersControllerHandler;
class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'Qchsoft\Buddiesplus\Components\LocationManager' => 'LocationManager'
        ];
    }
    
    public function registerSettings(){
        return [
            'countries' => [
                'label'       => 'Countries',
                'description' => 'Manage available user countries',
                'category'    => 'Locations',
                'icon'        => 'icon-globe',
                'url'         => Backend::url('qchsoft/buddiesplus/countries'),
                'order'       => 500,
                'keywords'    => 'countries',
                'permissions' => ["manage-locations"]
            ],
            'states' => [
                'label'       => 'States',
                'description' => 'Manage available user states.',
                'category'    => 'Locations',
                'icon'        => 'icon-university',
                'url'         => Backend::url('qchsoft/buddiesplus/states'),
                'order'       => 501,
                'keywords'    => 'states',
                'permissions' => ["manage-locations"]
            ],
            'cities' => [
                'label'       => 'Cities',
                'description' => 'Manage available user cities.',
                'category'    => 'Locations',
                'icon'        => 'icon-home',
                'url'         => Backend::url('qchsoft/buddiesplus/cities'),
                'order'       => 502,
                'keywords'    => 'cities',
                'permissions' => ["manage-locations"]
            ],
        ];
    }

    public function boot(){
        $this->addEventListener();
    }

    protected function addEventListener(){
        Event::subscribe(UserAddressModelHandler::class);
        Event::subscribe(UsersControllerHandler::class);
    }
}
