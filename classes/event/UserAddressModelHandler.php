<?php namespace Qchsoft\BuddiesPlus\Classes\Event;

use Lovata\Ordersshopaholic\Models\UserAddress as UserAddressModel;
class UserAddressModelHandler{

    public function subscribe(){
        UserAddressModel::extend(function($model) {
            $model->addFillable(["city_id"]);
        });
    }

}