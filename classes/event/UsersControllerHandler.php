<?php namespace Qchsoft\BuddiesPlus\Classes\Event;

use Lovata\Buddies\Controllers\Users as UsersController;
use Lovata\Buddies\Models\User\User as UserModel;
class UsersControllerHandler{

    public function subscribe(){

        UsersController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof UserModel) {
               
                return;
            }
           /* pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            if (!$model->exists) {
              
                return;
            }
            */
            $form->removeTab("lovata.buddies::lang.tab.tasks");   
            $form->removeField("is_activated");
                 
        });
    }
    
}