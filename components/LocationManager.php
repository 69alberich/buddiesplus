<?php namespace QchSoft\BuddiesPlus\Components;

use QchSoft\BuddiesPlus\Models\Country;
use QchSoft\BuddiesPlus\Models\State;
use QchSoft\BuddiesPlus\Models\City;
use Input;
class LocationManager extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Location Manager',
            'description' => 'Manage dropdowns of countries, states and cities.'
        ];
    }

    // This array becomes available on the page as {{ component.posts }}
    public function countries($countryId=null, $api=false){
        if(!$api){
            $countries = Country::select(["id", "name"]);
            if ($countryId) {
                $countries->where("id", $countryId);
            }
            return $countries->get();
        }else{
            $countries = $this->getCountriesFromApi();
            trace_log($countries);
            return $countries;
        }        
    }

    public function states($countryId=null){
        $states = State::select(["id", "name"]);
        if ($countryId) {
            $states->where("country_id", $countryId);
        }
        return $states->get();;
    }

    public function cities($stateId=null){
        $cities = City::select(["id", "name"]);
        if ($stateId) {
            $cities->where("state_id", $stateId);
        }
        return $cities->get();
    }

    public function onChangeCountry(){
        $data = post();
        $this->page['states'] = $this->states($data["country"]);
        //trace_log($data);
    }

    public function onChangeState(){
        $data = post();
        $this->page['cities'] = $this->cities($data["state"]);
        //trace_log($data);
    }

    public function onSelectSavedAddress(){
        $data = post();
        $this->page['selectedAddressId'] = $data["radio-address"];
    }


    public function getCountriesFromApi(){
    
        //next example will recieve all messages for specific conversation
        $service_url = 'https://restcountries.eu/rest/v2/all';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            //die('error occured: ' . $decoded->response->errormessage);
        }
        
        for ($i=0; $i < count($decoded); $i++) { 
            if($decoded[$i]->name == "Venezuela (Bolivarian Republic of)"){
                $decoded[$i]->name = "Venezuela";
            }
        }    
            
        
        return $decoded;
        //$this->page["paises"] = $decoded;
            
    }
}